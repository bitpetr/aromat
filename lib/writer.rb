module Writer
  def write(file)
    return unless file

    File.open("./files/#{@img_name}", 'wb') do |f|
      print '................ '

      if f.write file
        aromat = Aromat.new(title: @title, collection: @collection)
        if aromat.save
          extension = img_name.split('.').pop
          image = "#{aromat.id}.extension"
          aromat.update(image: image)
        end
        `mv ./files/#{@img_name} ./app/assets/images/#{@image_name}`
      end
    end
  end
end
