# require_relative '../app/models/aromat'
require 'nokogiri'
require 'open-uri'
require 'curb'
require_relative './downloader'
require_relative './writer'

class Scrapper
  
  include Writer

  attr_reader :base_uri

   def initialize(url)
    @base_uri = url
  end

  def scrap
    html = Nokogiri::HTML(open(@base_uri))

    html.css('.perfumeslist').each do |perfum| 
      img = perfum.at_css('img')['src']
      @img_name = img.split('/')[-1]
      @title = perfum.css('a').text.strip
      collection = perfum.css('span.mtext').text.split(' ')
      collection.shift
      @collection = collection.join(' ').strip
 
      write(Downloader.start(img))
    end
  end
end
