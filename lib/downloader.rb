class Downloader
  def self.start(track_uri)
    easy = Curl::Easy.new
    easy.follow_location = true
    easy.useragent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'
    easy.enable_cookies = true
    easy.url = track_uri
    
    easy.perform
    if (easy.response_code == 200)  && (easy.content_type[0..4] == 'image')
      easy.body_str
    else
      puts "\t\t4: пропуск, response_code: #{easy.response_code}, content_type: #{easy.content_type}"
      nil
    end
  end
end
