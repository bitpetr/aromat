class CreateAromats < ActiveRecord::Migration
  def change
    create_table :aromats do |t|
      t.string :title
      t.string :collection
      t.string :image

      t.timestamps
    end
  end
end
